package com.nakibul.androidtestapplication.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import com.nakibul.androidtestapplication.ui.adapter.GithubAdapter
import com.nakibul.androidtestapplication.R
import com.nakibul.androidtestapplication.utils.Status
import com.nakibul.androidtestapplication.viewmodel.GithubRepositoryListViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ListFragment : Fragment() {

    private val mainViewModel: GithubRepositoryListViewModel by viewModels()
    private lateinit var adapter: GithubAdapter
    private lateinit var recylerView: RecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var root:FrameLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        recylerView = view.findViewById(R.id.rvRepository)
        progressBar = view.findViewById(R.id.paginationProgressBar)
        root = view.findViewById(R.id.root)
        setupRecyclerView()

        adapter.setOnItemClickListener {
            Log.d("list of article>>>",it.toString())
            findNavController().navigate(R.id.action_listFragment_to_detailsFragment,
                bundleOf(
                    Pair(
                        "repository", Gson().toJson(
                            it
                        )
                    )
                )
            )
        }

        mainViewModel.resRepository.observe(viewLifecycleOwner, Observer { it ->
            when (it.status) {
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    recylerView.visibility = View.VISIBLE
                    it.data.let {
                        if (it?.total_count!! >= 0) {
                            it.items.let {
                                adapter.submitList(it)
                            }
                        } else {
                             Snackbar.make(root, "Status = false", Snackbar.LENGTH_SHORT).show()
                        }
                    }
                }

                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                    recylerView.visibility = View.GONE
                }

                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    recylerView.visibility = View.VISIBLE
                    Snackbar.make(root, "Something went wrong", Snackbar.LENGTH_SHORT).show()
                }
            }
        })

        return view
    }

    private fun setupRecyclerView() {
        adapter = GithubAdapter()
        recylerView.layoutManager = LinearLayoutManager(requireContext())
        recylerView.adapter = adapter
    }

}