package com.nakibul.androidtestapplication.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.gson.Gson

import com.nakibul.androidtestapplication.R
import com.nakibul.androidtestapplication.model.Item
import com.nakibul.androidtestapplication.utils.Constants


class DetailsFragment : Fragment() {

    private lateinit var itemObject: Item
    private lateinit var repoName: TextView
    private lateinit var repoDescription: TextView
    private lateinit var ownerName: TextView
    private lateinit var starCount: TextView
    private lateinit var forkCount: TextView
    private lateinit var mmddyyyyhhss: TextView
    private lateinit var titleImage: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val itemJson = arguments?.getString("repository")
        itemJson?.let {
            itemObject =
                Gson().fromJson(itemJson, Item::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_details, container, false)
        repoName = view.findViewById(R.id.repo_name)
        repoDescription = view.findViewById(R.id.repo_description)
        ownerName = view.findViewById(R.id.owner_name)
        starCount = view.findViewById(R.id.star_count)
        forkCount = view.findViewById(R.id.fork_count)
        mmddyyyyhhss = view.findViewById(R.id.last_updated)
        titleImage = view.findViewById(R.id.iv_image)


        repoName.text = "Name: ${itemObject.name}"
        repoDescription.text = "Description: ${itemObject.description}"
        ownerName.text = "Owner name: ${itemObject.owner.login}"
        starCount.text = "Stars: ${itemObject.stargazers_count}"
        forkCount.text = "Forks: ${itemObject.forks_count}"
        mmddyyyyhhss.text = "Last Updated: ${Constants.dateConverter(itemObject.updated_at)}"

        Glide.with(this).load(itemObject.owner.avatar_url).into(titleImage)



        return view
    }


}