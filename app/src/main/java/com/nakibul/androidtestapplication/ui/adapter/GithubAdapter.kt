package com.nakibul.androidtestapplication.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.nakibul.androidtestapplication.R
import com.nakibul.androidtestapplication.model.Item

class GithubAdapter : RecyclerView.Adapter<GithubAdapter.GithubViewHolder>() {
    inner class GithubViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val repoName: TextView = itemView.findViewById<TextView>(R.id.repo_name)
        val repoDescription: TextView = itemView.findViewById<TextView>(R.id.repo_description)
        val repoOwnerName: TextView = itemView.findViewById<TextView>(R.id.owner_name)
        val repoImage: ImageView = itemView.findViewById<ImageView>(R.id.avatar)
    }

    private val differCallback = object : DiffUtil.ItemCallback<Item>() {
        override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem.node_id == newItem.node_id
        }

        override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
            return oldItem == newItem
        }
    }

    private val differ = AsyncListDiffer(this, differCallback)
    fun submitList(list: List<Item>) = differ.submitList(list)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GithubViewHolder {
        return GithubViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_preview,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }
    private var onItemClickListener: ((Item) -> Unit)? = null
    override fun onBindViewHolder(holder: GithubViewHolder, position: Int) {
        val repo = differ.currentList[position]
        holder.itemView.apply {

            holder.repoName.text = repo.name
            holder.repoDescription.text = repo.description
            holder.repoOwnerName.text = repo.full_name
            Glide.with(context).load(repo.owner.avatar_url).into(holder.repoImage)
            setOnClickListener {
                Log.d("test", "ami adapter ")
                onItemClickListener?.let { it(repo) }
            }
        }
    }
    fun setOnItemClickListener(listener: (Item) -> Unit) {
        onItemClickListener = listener
    }
}