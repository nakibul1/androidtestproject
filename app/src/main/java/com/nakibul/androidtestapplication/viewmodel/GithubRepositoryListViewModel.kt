package com.nakibul.androidtestapplication.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nakibul.androidtestapplication.model.GithubRepositoryList
import com.nakibul.androidtestapplication.repository.GithubListRepository
import com.nakibul.androidtestapplication.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class GithubRepositoryListViewModel @Inject constructor(
    private val repository: GithubListRepository
) : ViewModel() {
    private val _resRepositroy = MutableLiveData<Resource<GithubRepositoryList>>()
    val resRepository: MutableLiveData<Resource<GithubRepositoryList>>
        get() = _resRepositroy

    init {
        getRepositoryList()
    }

    private fun getRepositoryList() = viewModelScope.launch {
        _resRepositroy.postValue(Resource.loading(null))
        repository.getRepositoryList().let {
            if (it.isSuccessful) {
                _resRepositroy.postValue(Resource.success(it.body()))
            } else {
                _resRepositroy.postValue(Resource.error(it.errorBody().toString(), null))
            }
        }
    }
}