package com.nakibul.androidtestapplication.repository

import com.nakibul.androidtestapplication.api.ApiHelper
import retrofit2.http.Headers
import javax.inject.Inject

class GithubListRepository @Inject constructor(
    private val apiHelper: ApiHelper
) {
    @Headers("X-AndroidName : com.nakibul.androidtestapplication")
    suspend fun getRepositoryList() = apiHelper.getRepositoryList()
}