package com.nakibul.androidtestapplication.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}