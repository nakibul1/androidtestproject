package com.nakibul.androidtestapplication.utils

import java.text.SimpleDateFormat
import java.util.Locale

object Constants {
    const val BASE_URL = "https://api.github.com/"

    fun dateConverter(dateTimeString:String):String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US)
        val outputFormat = SimpleDateFormat("MM-dd-yyyy HH:mm", Locale.US)
        val date = inputFormat.parse(dateTimeString)
        val formattedDate = outputFormat.format(date)
        return formattedDate
    }

}