package com.nakibul.androidtestapplication.api

import com.nakibul.androidtestapplication.model.GithubRepositoryList
import retrofit2.Response

interface ApiHelper {
    suspend fun getRepositoryList():Response<GithubRepositoryList>
}