package com.nakibul.androidtestapplication.api

import com.nakibul.androidtestapplication.model.GithubRepositoryList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("search/repositories")
    suspend fun getRepositoryList(
        @Query("q")
        q: String = "android",
        @Query("sort")
        sort: String = "stars",
        @Query("order")
        order: String = "desc"
    ): Response<GithubRepositoryList>
}