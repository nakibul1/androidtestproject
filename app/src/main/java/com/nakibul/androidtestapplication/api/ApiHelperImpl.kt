package com.nakibul.androidtestapplication.api

import com.nakibul.androidtestapplication.model.GithubRepositoryList
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(
    private val apiService: ApiService
) : ApiHelper {
    override suspend fun getRepositoryList(): Response<GithubRepositoryList> =
        apiService.getRepositoryList()

}