package com.nakibul.androidtestapplication.model

data class GithubRepositoryList(
    val incomplete_results: Boolean,
    val items: List<Item>,
    val total_count: Int
)